package com.duy.quizzapp.Service;

import com.duy.quizzapp.Entity.Question;
import org.springframework.stereotype.Service;

@Service
public interface QuestionService {
    void saveQuestion(Question question);

    void deleteQuestion(Long questionId);
}
