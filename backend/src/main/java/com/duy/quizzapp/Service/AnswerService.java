package com.duy.quizzapp.Service;

import com.duy.quizzapp.Entity.Answer;
import org.springframework.stereotype.Service;

@Service

public interface AnswerService {

    void saveAnswer(Answer answer);
    void deleteAnswer(int id);
}
