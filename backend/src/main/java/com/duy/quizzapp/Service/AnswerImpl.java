package com.duy.quizzapp.Service;

import com.duy.quizzapp.Entity.Answer;
import com.duy.quizzapp.Repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnswerImpl implements AnswerService{

    private final AnswerRepository answerRepository;
    @Autowired
    private AnswerImpl( AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public void saveAnswer(Answer answer) {
        answerRepository.save(answer);
    }

    @Override
    public void deleteAnswer(int id) {
        answerRepository.deleteById(Long.valueOf("16"));
    }
}
