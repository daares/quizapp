package com.duy.quizzapp.Service;

import com.duy.quizzapp.Entity.Quiz;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface QuizService {
    List<Quiz> getAllQuizzes();
    Quiz getQuizById(Long id);
    void saveQuiz(Quiz quiz);
    void deleteQuiz(Long id);
}

