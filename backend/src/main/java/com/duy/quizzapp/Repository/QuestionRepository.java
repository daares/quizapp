package com.duy.quizzapp.Repository;

import com.duy.quizzapp.Entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
