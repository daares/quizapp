package com.duy.quizzapp.Repository;

import com.duy.quizzapp.Entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
