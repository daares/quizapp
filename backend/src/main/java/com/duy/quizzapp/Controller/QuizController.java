package com.duy.quizzapp.Controller;

import com.duy.quizzapp.Entity.Answer;
import com.duy.quizzapp.Entity.Question;
import com.duy.quizzapp.Entity.Quiz;
import com.duy.quizzapp.Mapping.QuizRequestBody;
import com.duy.quizzapp.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class QuizController {
    @Autowired
    private QuizServiceImpl quizService;

    @Autowired
    private QuestionImpl questionService;

    @Autowired
    private AnswerImpl answerService;

    @GetMapping("/")
    public String test(){
        return "test string";
    }
    @PostMapping("/addquiz")
    public void addQuiz(@RequestBody QuizRequestBody quizRequestBody) {
        Quiz quiz = new Quiz("test", "test");
        quizService.saveQuiz(quiz);
        Question question = new Question("test", "test", quiz);
        questionService.saveQuestion(question);
        Answer answer = new Answer("answer", false, question );
        answerService.saveAnswer(answer);
    }

}
