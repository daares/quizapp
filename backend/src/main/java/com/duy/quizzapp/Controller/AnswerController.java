package com.duy.quizzapp.Controller;

import com.duy.quizzapp.Service.AnswerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class AnswerController {

    @Autowired
    public AnswerImpl answerService;

    @DeleteMapping("/deleteanswer")
    public void delete(){
        answerService.deleteAnswer(11);
    }
}
