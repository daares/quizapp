package com.duy.quizzapp.Controller;

import com.duy.quizzapp.Entity.Question;
import com.duy.quizzapp.Service.QuestionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class QuestionController {

    @Autowired
    QuestionImpl questionservice;

    @DeleteMapping("/deletequestion")
    public void deleteQuestion(@RequestBody Question questionId) {
        questionservice.deleteQuestion(Long.valueOf("14"));
    }
}